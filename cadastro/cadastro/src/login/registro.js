import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, Button, View} from 'react-native';
import firebase from 'react-native-firebase';


class Registro extends Component {
  constructor (props){
    super(props)
    this.state = {
      colocarEmail: '',
      colocarSenha: '',
    };
  }

    SalvarCadastro = async () => {
        const { colocarEmail, colocarSenha } = this.state;

        if ((colocarEmail == '') || (colocarSenha == '') || (colocarEmail == '' && colocarSenha == '')) {
            Alert.alert('Faltam dados para terminar o cadastro');
        }
        else if (colocarEmail != '' && colocarSenha != '') {
            firebase.auth().createUserWithEmailAndPassword(colocarEmail, colocarSenha)
            Alert.alert('Cadastro Realizado com Sucesso');
            this.props.navigation.navigate('Tela1')
        }

  }

 render() {
    return (
    <View style={styles.container}>
                <Text 
                    style={styles.texto}>
                    Cadastro
                </Text>                
                <TextInput placeholder='Digite seu email: ' 
                        keyboardType='email-address'                
                        autoCapitalize="none"
                        onChangeText={(colocarEmail) => this.setState({ colocarEmail })}
                />                
                <TextInput secureTextEntry={true} placeholder='Digite sua senha: ' 
                        onChangeText={(colocarSenha) => this.setState({ colocarSenha })}
                />
                <View style={styles.botao2}>
                <Button 
                  title="Cadastrar"
                  onPress={this.SalvarCadastro}
                />
                      
                </View> 
    </View>                 
            )
    }
}


const styles = StyleSheet.create({
  container: {
  padding: 20,
    backgroundColor: '#F5FCFF',
  },
  texto: {
    fontSize: 27,
    textAlign: "center",
  },
  botao: {
  margin:7,
  },
  botao2: {
  margin:10,
  },  
});

Registro.navigationOptions = {
  header: null,
};


export default Registro;

