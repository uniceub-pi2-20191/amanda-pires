import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, Button, View} from 'react-native';
import firebase from 'react-native-firebase';

class Inicial extends Component {
  constructor (props){
    super(props)

      this.state = {
        colocarEmail: '',
        colocarSenha: '',
    };
  
  }

  validarRegistro = async () => {

      const {colocarEmail, colocarSenha } = this.state;
      if ((colocarEmail == '' && colocarSenha == "") || (colocarEmail == '' || colocarSenha == "")) {
        Alert.alert('E-mail usuário e/ou senha.');

      }
      else {
        try {
          await firebase.auth().signInWithEmailAndPassword(colocarEmail,colocarSenha);
          this.props.navigation.navigate('Tela3')
        }
              catch(err){
                  Alert.alert('E-mail ou senha invalidos.');  
            } 
    }
    }


  render() {
    return (
    <View style={styles.container}>
                <Text 
                    style={styles.texto}>
                    Login
                </Text>
                <TextInput placeholder='E-mail: ' 
                    autoCorrect={false}
                    keyboardType='email-address'
                    onChangeText={colocarEmail => this.setState({ colocarEmail })}
                />
                <TextInput secureTextEntry={true} placeholder='Senha: '
                    onChangeText={colocarSenha => this.setState({ colocarSenha })}
                />
                <View style={styles.botao} >
                <Button 
                  title="Login"
                  onPress={this.validarRegistro}
                />
                      
                  </View>                 
                <View style={styles.botao2}>
                <Button 
                  title="Cadastrar"
                  onPress={() => this.props.navigation.navigate('Tela2')}
                />
                      
                  </View> 
                  </View>                 
            )
    }
}

const styles = StyleSheet.create({
  container: {
  padding: 20,
    backgroundColor: '#F5FCFF',
  },
  texto: {
    fontSize: 27,
    textAlign: "center",
  },
  botao: {
  margin:7,
  },
  botao2: {
  margin:10,
  },  
});

Inicial.navigationOptions = {
  header: null,
};


export default Inicial;
